/* Load env variables. */
import * as path from 'path';
import * as dotenv from 'dotenv';
dotenv.config({ path: path.resolve(process.cwd(), '.env.development') })

/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

export const pathPrefix = '/' + (process.env.BLOG_PATH_PREFIX || '')

type Plugin = (
  | string
  | { resolve: string, options: Record<string, unknown> }
)

export const plugins: Plugin[] = [
  {
    resolve: "gatsby-plugin-graphql-codegen",
    options: {
      fileName: "./typings/graphql-codegen.d.ts"
    }
  },
  "gatsby-plugin-react-helmet",
  "gatsby-plugin-sass",
  `gatsby-plugin-image`,
  `gatsby-plugin-sharp`,
  `gatsby-transformer-sharp`,
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      path: `${__dirname}/content/blog`,
      name: `blog`,
    },
  },
  {
    // parse images from src to allow querying them via GraphQL
    resolve: `gatsby-source-filesystem`,
    options: {
      path: `${__dirname}/src/images`,
      name: `images`,
    },
  },
  `gatsby-remark-images`,
  {
    resolve: `gatsby-plugin-mdx`,
    options: {
      // a workaround to solve mdx-remark plugin compat issue
      // https://github.com/gatsbyjs/gatsby/issues/15486
      plugins: [
        {
          resolve: `gatsby-remark-images-medium-zoom`, // Important!
          options: {
            background: 'rgba(255, 255, 255, 0.95)',
          },
        },
      ],
      gatsbyRemarkPlugins: [
        `gatsby-remark-prismjs`,
        {
          resolve: `gatsby-remark-images`,
          options: {
            maxWidth: 632, // blog content width
            linkImagesToOriginal: false
          },
        },
        `gatsby-remark-images-medium-zoom`,
        `gatsby-remark-smartypants`,
        `gatsby-remark-copy-linked-files`,
      ],
    },
  },
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: `Life Journal`,
      short_name: `Blog`,
      start_url: `/`,
      background_color: `#ffffff`,
      theme_color: `#000000`,
      display: `standalone`,
      icon: `src/images/favicon.png`, // path relative to the root of the site
    },
  },
  `gatsby-plugin-offline`, // should be after manifest plugin
]

export const siteMetadata = {
  title: `Life Journal`,
  description: `No description required`,
  author: {
    name: `andrzej1_1`,
    summary: `lives and works on Earth building <strike>useful</strike> things.`,
  }
}
