import * as path from 'path';

import { GatsbyNode, CreatePagesArgs, CreateNodeArgs, CreateSchemaCustomizationArgs } from 'gatsby';

import { getBlogPosts, getPostUrl } from "./src/helpers/blog"

import { BlogPostsQuery, Mdx } from "./typings/graphql-codegen"

export const onCreateNode: GatsbyNode['onCreateNode'] = ({ node, actions }: CreateNodeArgs) => {

  const { createNodeField } = actions

  /* Generate slug for Mdx nodes. */
  if (node.internal.type === `Mdx`) {
    const slugVal = path.basename(path.dirname((node as unknown as Mdx).fileAbsolutePath))

    createNodeField({
      node,
      name: `slug`,
      value: slugVal
    })
  }
}

export const createPages: GatsbyNode['createPages'] = async ({ graphql, actions, reporter }: CreatePagesArgs) => {
  const { createPage } = actions

  // Define a template for blog post
  const blogPost = path.resolve(`./src/templates/blog-post.tsx`)

  // Get all markdown blog posts sorted by date
  const result = await getBlogPosts<BlogPostsQuery>({ graphql });

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your blog posts`,
      result.errors
    )
    return
  }

  const posts = result.data?.allMdx.nodes || []

  // Create blog posts pages from mdx files found at "content/blog" (defined in gatsby-config.ts)
  // `context` is available in the template as a prop and as a variable in GraphQL
  posts.forEach((post, index) => {

    /* Obtain prev/next post id. */
    let previousPostId: string|null = null;
    let nextPostId: string|null = null;
    if (index > 0) {
      const prevPost = posts[index - 1];
      if (prevPost !== undefined) {
        previousPostId = prevPost.id;
      }
    }
    if ((index+1) < posts.length) {
      const nextPost = posts[index + 1];
      if (nextPost !== undefined) {
        nextPostId = nextPost.id;
      }
    }

    createPage({
      path: getPostUrl(post.fields.slug),
      component: blogPost,
      context: {
        /* Note: context props are passed as variables to component (blogPost) graphql query. */
        id: post.id,
        previousPostId,
        nextPostId,
      },
    })
  })
}

export const createSchemaCustomization: GatsbyNode['createSchemaCustomization'] = ({ actions }: CreateSchemaCustomizationArgs) => {
  const { createTypes } = actions;
  const typeDefs = `
    type Site implements Node {
      siteMetadata: SiteSiteMetadata!
    }
    type SiteSiteMetadata {
      title: String!
      description: String!
      author: SiteSiteMetadataAuthor!
    }
    type SiteSiteMetadataAuthor {
      name: String!
      summary: String!
    }
    type Mdx implements Node {
      frontmatter: MdxFrontmatter!
      fields: MdxFields!
    }
    type MdxFrontmatter {
      title: String!
      date: Date! @dateformat
      description: String!
    }
    type MdxFields {
      slug: String!
    }
  `;
  createTypes(typeDefs);
};
