export type GraphQLResult<TData> = Promise<{errors?: any, data?: TData}>

export interface GetBlogPostsArgs<TData> {
  graphql<TVariables = any>(query: string, variables?: TVariables | undefined): GraphQLResult<TData>
}

export const getBlogPosts = async <TData>({ graphql }: GetBlogPostsArgs<TData>): GraphQLResult<TData> => {
  // Get all markdown blog posts sorted by date
  const result = await graphql(`
    query BlogPosts {
      allMdx(
        sort: { fields: [frontmatter___date], order: ASC }
        limit: 1000
      ) {
        nodes {
          id
          fields {
            slug
          }
        }
      }
    }
  `)

  return result;
}

export const getPostUrl = (slug: string): string => {
  /* Note: Previously I used /blog/${slug}, but it did not
   * work well due to Gatsby prefix issue - see following link:
   * > https://github.com/gatsbyjs/gatsby/issues/27604#issuecomment-888478557 */
  return `/post/${slug}`
}
