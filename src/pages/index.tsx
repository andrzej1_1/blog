import { Link, graphql , PageProps } from "gatsby"
import HTMLReactParser from 'html-react-parser';

import * as React from "react"
import { IndexQuery } from "../../typings/graphql-codegen"

import Bio from "../components/bio"
import Head from "../components/head"
import Layout from "../components/layout"

import { getPostUrl } from "../helpers/blog"

import * as styles from './index.module.scss'

export const query = graphql`
  query Index {
    site {
      siteMetadata {
        title
      }
    }
    allMdx(sort: { fields: [frontmatter___date], order: DESC }) {
      nodes {
        excerpt
        fields {
          slug
        }
        frontmatter {
          date(formatString: "MMMM DD, YYYY")
          title
          description
        }
      }
    }
  }
`

const IndexPage = ({ data, location }: PageProps<IndexQuery>): JSX.Element => {
  const siteTitle = data.site?.siteMetadata.title || "";
  const posts = data.allMdx.nodes
  let postsContent: JSX.Element

  if (posts.length === 0) {
    postsContent = (
      <p>No blog posts added yet.</p>
    )
  } else {
    postsContent = (
      <ol style={{ listStyle: `none` }}>
        {posts.map(post => {
          const title = post.frontmatter.title || post.fields.slug

          return (
            <li key={post.fields.slug}>
              <article
                className={styles.postListItem}
                itemScope
                itemType="http://schema.org/Article"
              >
                <header>
                  <h2>
                    <Link to={getPostUrl(post.fields.slug)} itemProp="url">
                      <span itemProp="headline">{title}</span>
                    </Link>
                  </h2>
                  <small>{post.frontmatter.date}</small>
                </header>
                <section>
                  { HTMLReactParser(post.frontmatter.description || post.excerpt) }
                </section>
              </article>
            </li>
          )
        })}
      </ol>
    )
  }

  return (
    <Layout location={location} title={siteTitle}>
      <Head title="All posts" />
      <Bio />
      {postsContent}
    </Layout>
  )
}

export default IndexPage
