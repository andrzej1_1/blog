import { graphql , PageProps } from "gatsby"

import * as React from "react"
import { NotFoundQuery } from "../../typings/graphql-codegen"

import Head from "../components/head"
import Layout from "../components/layout"

export const query = graphql`
  query NotFound {
    site {
      siteMetadata {
        title
      }
    }
  }
`

const NotFoundPage = ({ data, location }: PageProps<NotFoundQuery>): JSX.Element => {
  const siteTitle = data.site?.siteMetadata.title || "";

  return (
    <Layout location={location} title={siteTitle}>
      <Head title="Error 404" />
      <span rel-page-title={siteTitle}></span>
      <h1>404: Not Found</h1>
      <p>You just hit a route that doesn't exist... the sadness.</p>
    </Layout>
  )
}

export default NotFoundPage
