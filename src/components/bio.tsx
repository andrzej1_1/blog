

import { useStaticQuery, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import HTMLReactParser from 'html-react-parser';
import * as React from "react"

import { BioQuery } from "../../typings/graphql-codegen"

import * as styles from './bio.module.scss'

const Bio = (): JSX.Element => {

  const data: BioQuery = useStaticQuery(graphql`
    query Bio {
      site {
        siteMetadata {
          author {
            name
            summary
          }
        }
      }
    }
  `)

  const author = data.site?.siteMetadata.author;

  return (
    <div className={styles.bio}>
      <StaticImage
        className={styles.bioAvatar}
        layout="fixed"
        formats={["auto", "webp", "avif"]}
        src="../images/profile-pic.webp"
        width={50}
        height={50}
        quality={95}
        alt="Profile picture"
      />
      {author && (
        <p>
          Written by <strong>{author.name}</strong> who { HTMLReactParser(author.summary) }
        </p>
      )}
    </div>
  )
}

export default Bio
