import { useStaticQuery, graphql } from "gatsby"
import * as React from "react"
import { Helmet } from "react-helmet"
import { HeadQuery } from "../../typings/graphql-codegen"

interface HeadProps {
  title: string
  description?: string
  lang?: string
}

const Head = ({ title, description, lang }: HeadProps): JSX.Element => {

  const data: HeadQuery = useStaticQuery(graphql`
    query Head {
      site {
        siteMetadata {
          title
          description
        }
      }
    }
  `)

  const metaTitle = data.site?.siteMetadata.title
  const metaDescription = description || data.site?.siteMetadata.description

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s | ${metaTitle}`}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
      ]}
    />
  )
}

export default Head
