import { WindowLocation } from "@reach/router"
import { Link } from 'gatsby'
import * as React from "react"

import * as styles from './layout.module.scss'

interface LayoutProps {
  location: WindowLocation
  title: string
  children?: any
}

const Layout = ({ location, title, children }: LayoutProps): JSX.Element => {

  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  let header

  if (isRootPath) {
    header = (
      <h1 className={styles.mainHeading}>
        <Link to="/">{title}</Link>
      </h1>
    )
  } else {
    header = (
      <Link to="/" className={styles.headerLinkHome}>
        {title}
      </Link>
    )
  }

  return (
    <div className={styles.globalWrapper} data-is-root-path={isRootPath}>
      <header className={styles.globalHeader}>{header}</header>
      <main>{children}</main>
      <footer>
        © {new Date().getFullYear()}, Built with
        {` `}
        <a href="https://www.gatsbyjs.com">Gatsby</a>
      </footer>
    </div>
  )
}

export default Layout
