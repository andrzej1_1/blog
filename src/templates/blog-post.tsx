import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Link, graphql , PageProps } from "gatsby"

import { MDXRenderer } from "gatsby-plugin-mdx"

import * as React from "react"
import { BlogPostByIdQuery, BlogPostByIdQueryVariables } from "../../typings/graphql-codegen"

import Bio from "../components/bio"
import Head from "../components/head"
import Layout from "../components/layout"

import { getPostUrl } from "../helpers/blog"

import * as styles from './blog-post.module.scss'

export const query = graphql`
  query BlogPostById(
    $id: String!
    $previousPostId: String
    $nextPostId: String
  ) {
    site {
      siteMetadata {
        title
      }
    }
    mdx(id: { eq: $id }) {
      id
      excerpt(pruneLength: 160)
      body
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
      }
    }
    previous: mdx(id: { eq: $previousPostId }) {
      fields {
        slug
      }
      frontmatter {
        title
      }
    }
    next: mdx(id: { eq: $nextPostId }) {
      fields {
        slug
      }
      frontmatter {
        title
      }
    }
  }
`

const BlogPostTemplate = ({ data, location }: PageProps<BlogPostByIdQuery, BlogPostByIdQueryVariables>): JSX.Element => {
  if (data.mdx === undefined || data.mdx === null) {
    return <div>Unable to render post, no 'mdx' property.</div>
  }

  const post = data.mdx
  const siteTitle = data.site?.siteMetadata.title || `Title`
  const { previous, next } = data

  return (
    <Layout location={location} title={siteTitle}>
      <Head title={post.frontmatter.title} description={post.frontmatter.description || post.excerpt} />
      <article
        className={styles.blogPost}
        itemScope
        itemType="http://schema.org/Article"
      >
        <header>
          <h1 itemProp="headline">{post.frontmatter.title}</h1>
          <p>{post.frontmatter.date}</p>
        </header>
        <MDXRenderer itemProp="articleBody">{post.body}</MDXRenderer>
        <hr />
        <footer>
          <Bio />
        </footer>
      </article>
      <nav className={styles.blogPostNav}>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <Link to={getPostUrl(previous.fields.slug)} rel="prev">
                <FontAwesomeIcon icon={faArrowLeft} /> {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={getPostUrl(next.fields.slug)} rel="next">
                {next.frontmatter.title} <FontAwesomeIcon icon={faArrowRight} />
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  )
}

export default BlogPostTemplate
