# 📔 Personal Blog

This is code of personal blog, created using *Gatsby* framework. It is mostly based on [gatsby-starter-blog](https://github.com/gatsbyjs/gatsby-starter-blog), however I recreated it from scratch with *TypeScript* support as my primary goal. It is simplified a little, as I decided to not use SEO, social-related stuff or plugins for *RSS*, *Gastby Cloud*.

## 🚀 Build

Configure following environment variables:

 - BLOG_PATH_PREFIX - site prefix path, e.g. `blog/here` for site hosted at `server.dev/blog/here`; by default empty which means we are using root URL

Then simply run:

```console
$ yarn run build
```

Generated pages will be stored under `public` directory.

## ⚙️ TypeScript support

### Background

There is no native TypeScript support, as *Gatsby* was written in JavaScript, however there is currently ongoing progress with rewriting code base - see [issue](https://github.com/gatsbyjs/gatsby/issues/21995)).

Not yet ago we could use `ts-node` + `source-map-support` + `esm`, however *Gatsby* recently [became multiprocess](https://github.com/gatsbyjs/gatsby/pull/22759) and it is no longer working.

However there is **partial** TypeScript support with `gatsby-plugin-typescript` plugin, which is enabled by default. It is only partial, because code is transpiled (not compiled), so errors will not be detected.

### Type checking

To detect errors we can add *tsconfig.json* and run `tsc` with `--noEmit` option. Such config will be picked up by IDE and errors will be detected during file save.

### Config files

We could use `ts-node` to load `gatsby-config.ts` from `gatsby-config.js`, however this will not work for other files like `gatsby-node.ts` or `gatsby-browser.ts`.

However we can use pre-build script to compile .ts config files - see https://www.extensive.one/converting-gatsby-config-and-node-api-to-typescript/.

## 🔥 Troubleshooting

If you get errors in deps types when running `tsc`, then you should make sure yarn deps are deduplicated. To do so remove `yarn.lock` and `node_modules`, then run `yarn install` as it performs deduplication.

Reference: https://stackoverflow.com/questions/52399839/duplicate-identifier-librarymanagedattributes

Note: Disabling 'skipLibCheck' is just hack and it is not recommended.
