// custom typefaces
import "@fontsource/montserrat/latin.css"
import "@fontsource/merriweather/latin.css"
// normalize CSS across browsers
import "normalize.css"
// global CSS styles
import './src/style-common.scss'

// syntax highlighting + line highlighting + line numbering + shell prompt
import './src/style-prismjs.scss'
import "prism-themes/themes/prism-night-owl.css"
import "prismjs/plugins/line-numbers/prism-line-numbers.css"
import "prismjs/plugins/command-line/prism-command-line.css"
